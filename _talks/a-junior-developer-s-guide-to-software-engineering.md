---
abstract: New developers often ask, "How much Python do I need to know to get a job?".
  They overlook the other skills needed to be a successful software engineer. This
  talk highlights the other skill and tools needed, which can be used from the start
  of your coding journey.
level: Beginner
speakers:
- Kojo Idrissa
title: A Junior Developer's Guide to Software Engineering
type: talk
---

As people try to switch careers and become software engineers, they often focus *solely* on learning to program. However, there's more to being a professional developer or open source contributor than code. This talk highlights the what, when, and why of these other critical skills:

-  Version Control
-  Documentation
-  Testing & Test-Driven Development
-  Dependency Management & Deployment
-  Knowing Your Development Environment

This talk is for people trying to get started on the path to becoming a developer, as well as more experienced developers who're trying to guide or mentor those people.